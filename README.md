# newpipe-testing-framework

## Overview
This repository contains a set of UI tests for [NewPipe](https://github.com/TeamNewPipe/NewPipe), a "libre lightweight streaming front-end for Android". They were created for the purposes of a project assignment for the "Methods and Techniques of Software Testing" course at the Faculty of Electrical Engineering, Computer Science and Information Technology Osijek. 

Implemented using the following test libraries: 
* [TestNG](https://testng.org/doc/) - testing framework inspired by JUnit and NUnit 
* [Appium](http://appium.io/) - open source test automation framework for Android, iOS and Windows apps using the WebDriver protocol.

## Environment setup
This section explains the setup process needed in order to run these tests on your PC
. Unfortunately, I do not own a PC
 running macOS, but the setup process should be fairly similar to that on Linux. 

### 1. Installing Android Studio and the SDK tools
1. Download and install Android Studio ([link to website](https://developer.android.com/studio/index.html))
2. Run Android Studio and complete the setup process
3. Locate where Android Studio installed the Sdk. On Linux, this is (by default) `/home/username/Android/Sdk`, but it is most likely different on your PC
. From now on, I will refer to this path as `ANDROID_SDK_PATH`.<br/>

### 2. Installing the JDK (Java Development Kit)
1. Download the latest OpenJDK (at time of writing, [OpenJDK 13](https://jdk.java.net/13/))
2. Extract it somewhere meaningful (mine is at `/home/username/Documents/jdk-13.0.2`)
3. From now on, I will refer to `/home/username/Documents/jdk-13.0.2/bin/` as `JAVA_HOME`. Replace it with the corresponding path on your PC.
4. Export `JAVA_HOME` to path ([tutorial](https://docs.oracle.com/cd/E19062-01/sun.mgmt.ctr36/819-5418/gaznb/index.html))

### 3. Setting up an Android Device to run the tests on
If you own a physical Android device, connect it to your PC
 and skip to step 5. 

1. Run Android Studio
2. Start the AVD (Android Virtual Device) Manager
3. Create a new virtual device ([tutorial](https://developer.android.com/studio/run/managing-avds))
4. Start the virtual device and wait for it to boot up
5. Navigate to Settings on the device and turn on USB Debugging (you'll need to enable Developer Options to access these settings by tapping the Build Number button in the Settings 7 or so times) ([tutorial](https://developer.android.com/studio/debug/dev-options))
6. Remember the Android version you installed/already have installed on your device. From now on, this is referred to as `ANDROID_VERSION` (e.g. "9.0")

### 4. Starting the ADB server
In your terminal:
1. `cd ANDROID_SDK_PATH/platform-tools`
2. `./adb devices`  

If 2. doesn't work, try `chmod +x adb` and repeat.
You should see an output similar to this:
```
List of devices attached
emulator-5554	device
```
If you don't see a device (the list is empty) make sure you've turned on USB debugging and restart the emulator/reconnect your device to the PC.

Remember the `emulator-5554` part. This is the name of your connected device. From now on referred to as `DEVICE_NAME`

3. `./adb start-server`

You should see an output similar to this:
```
* daemon not running; starting now at tcp:5037
* daemon started successfully
```

### 5. Installing and configuring Appium
1. Download and install Appium ([link to website](http://appium.io/))
2. Start Appium and click the "Advanced" tab
3. Find the "Relaxed security" checkbox and check it. This will allow the testing framework to clean up downloaded files that accumulated when testing.
4. Now click the "Edit Configurations" button at the bottom of the Appium dialog
5. In the ANDROID_HOME field, paste `ANDROID_SDK_PATH` (step 1.3.)
6. In the JAVA_HOME field, paste `JAVA_HOME` (step 2.3.)
7. Click on "Save and Restart"

### 6. Download this repository
1. Download and install Git ([link to website](https://git-scm.com/downloads))
2. `git clone https://gitlab.com/davidtakac/newpipe-testing-framework.git`

### 7. Download the NewPipe .apk
1. Download NewPipe v0.18.2 ([link](https://github.com/TeamNewPipe/NewPipe/releases/tag/v0.18.2))
2. Save wherever you want. From now on, `path_to_newpipe_download/NewPipe_v0.18.2.apk` is `APK_LOCATION`

### 7. Install IntelliJ Idea and configure the project
1. Download and install IntelliJ Idea ([link to website](https://www.jetbrains.com/idea/download/))
2. Open the cloned project
3. Go to File -> Project Structure -> Platform Settings -> SDK and set JDK Home Path to `JAVA_HOME`
4. When the prompt comes up, confirm that you want to Auto-Import dependencies
5. Navigate to src/test/java/common/Constants.java and plug in your own setup variables (from this tutorial)
```JAVA
public class Constants {
    public static final String APK_LOCATION = "APK_LOCATION";
    public static final String ANDROID_VERSION = "ANDROID_VERSION";
    public static final String DEVICE_NAME = "DEVICE_NAME";
    public static final String REMOTE_ADDRESS = "http://127.0.0.1:4723/wd/hub";
}
```

### 8. Start the Appium server
1. Run Appium 
2. Click on "Start server"
3. Congrats, you started the server

### 9. Summing up
At this point, you should have:

* Appium server started and running (steps 5 and 8)
* The project open in IntelliJ with the `Constants.java` file set up to your variables (step 7.5.) and JDK home set to `JAVA_HOME` (step 7.3.)
* ADB server running (step 4.3.)
* Emulator or physical Android device with USB debugging turned on (step 3). The device should be unlocked with the screen active 

## Running the tests
That was quite a long setup process, wasn't it? The good news is - it's over and if you did everything right, you're ready to run the tests. 

Open IntelliJ, navigate to `src/test/java/` (using the Project Explorer in IntelliJ) and right-click the `tests` folder. Select `Run 'Tests in "tests"'`, sit back and watch the framework do these tests for you. 

## Test description and overview

### Code structure overview
Writing these tests with the bare Selenium and Appium methods proved to be a quite tedious and repetitive process. For instance, in order to click on any UI element, you would first need to wait for it to load. Then, when it loads, you would need to "extract" it into a variable using `AndroidDriver#findElement`. And then, finally, you could call `Element#click`. 

Since these are UI tests, waiting for elements to be visible and then clicking them is a very common occurrence. Having to write:
```JAVA
WebDriverWait wait = new WebDriverWait(driver, 15);
wait.until(ExpectedConditions.visibilityOfElementLocated(MobileBy.AccessibilityId("Add To Playlist")));
MobileElement addButton = (MobileElement) driver.findElementByAccessibilityId("Add To Playlist");
addButton.click();

```
Each time you need to click an element is redundant. Similarly, having to include the `AndroidDriver driver` attribute and the `@BeforeClass` setUp method in each new Test class is redundant. 

To solve these issues, I made the `BaseTest` class which abstracts the boilerplate behavior, leaving child classes with only the responsibility of implementing their `@Test` methods. 

Each Test class extends the `BaseTest` which provides some useful methods that significantly reduce boilerplate. For instance, the ugly code above now becomes:  
`findElementWhenVisible(MobileBy.AccessibilityId("Add To Playlist)).click();`

After each test, the `@AfterMethod` method presses the back button (which closes the notification shade) and restarts the app. 

### 1. Download test
One of the main features of the NewPipe application is background playback of videos and the Download feature. It allows users to download any video they want in the desired quality. Users can even download just the audio as an .mp3 file. Because of this, it is one of the critical features that **need to work** in order for the application to be of worth to it's users. 

The test opens the application and clicks on a video in the Trending feed. When the video opens, it clicks the download button and allows the "Access storage" permission. Then it enters a random file name and starts the download. 

Then it opens the notification shade and waits for the "Download finished" notification. After it arrives, it checks to see whether or not the random file name exists in local storage. If it does, the video was successfully downloaded and the test passes. 

For the teardown part, I used the `mobile:shell` script execution method of the `AndroidDriver` class to delete the file from local storage since the file isn't needed when the test completes. 

**Note**: Every once in a while, a really long video will appear as the first video on the list. If you don't want to wait a long time for the video to download, you should change the `xpath_video` variable to open a different video (instructions in code comments).

### 2. Add video to playlist and delete playlist test
Adding videos to playlists is one of the main YouTube features in general, not just of NewPipe. But since NewPipe doesn't have access to the YouTube API, it needs to emulate this behavior. 

The test opens the application and clicks on the first video in the Trending page (the video it clicks doesn't really matter). It waits for the video to load and then clicks the "Add To Playlist" button. It generates a random name and clicks on "Create". 

Then it navigates back to the home page, clicks the Playlist tab and checks to see if the playlist was created. If it was, it long-clicks the playlist and deletes it and then checks to see if it was deleted. If all these checks passed, the test passes. 

### 3. Background playback test
As mentioned before, one of the main features of NewPipe is it's ability to play YouTube videos in the background. 

The test (again) clicks the first video in the Trending page. It waits for the video to load, clicks the "Background" button and opens the notification shade. It then asserts whether or not the "Play/pause" NewPipe button is visible. If it is, the test passes. 

### 4. Search test
Needless to say, with the abundance of YouTube videos ([5+ billion](https://www.omnicoreagency.com/youtube-statistics/)), it is of critical importance to be able to search for videos and/or content creators you wish to see. 

This test clicks the search icon, clears the input field, writes "idubbbz" and sends the `AndroidKey.ENTER` event which triggers the search. 

It then waits for the page to load and sets the filter to "Channels". This test expects "iDubbbzTV" to be one of the top results because it is a very popular channel and the search term "idubbbz" is similar enough to the channel name, which should therefore be no problem for the app to locate. 

If the test finds the "iDubbbzTV" text on the page, the test passes. 

**Note:** Given the dynamic nature of YouTube and changing/disappearing channels, this test could fail in the future if iDubbbzTV is removed or renamed to something else. In that case, you should edit the `searchTerm` and `expectedSearchResult` to something more up-to-date. 

### 5. License test
This is a simple test that asserts whether or not the GNU General Public License dialog pops up when the user clicks on the corresponding button in the About -> Licenses tab. 

The tests clicks the hamburger menu icon and waits for the drawer to pop up. Then it clicks the "About" button, again waiting for the "Licenses" tab to appear. When it does, it clicks on it and then clicks the "Read license" button. 

It waits for the License dialog to pop up and asserts whether or not the dialog title is "GNU General Public License, Version 3.0"

## Known issues and how to solve them
1. **`org.openqa.selenium.WebDriverException: An unknown server-side error occurred while processing the command. Original error: Could not proxy command to remote server. Original error: Error: socket hang up`**  
Rerun the failed tests. If this doesn't work, restart Appium and the ADB server and try again. The tests didn't fail because they failed, but because Appium is acting up. I haven't figured out a better solution or the cause to this error.
2. **`org.openqa.selenium.WebDriverException: An unknown server-side error occurred while processing the command. Original error: Potentially insecure feature 'adb_shell' has not been enabled.`**  
Stop the Appium server, enable the Relaxed security feature (step 5.3.) and start the server again. Rerun the failed test. 