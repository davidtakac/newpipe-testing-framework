package tests;

import base.BaseTest;
import com.google.common.collect.ImmutableMap;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@SuppressWarnings("FieldCanBeLocal")
public class NewPipeTests extends BaseTest {

    private String firstVideoXpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout" +
            "/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout" +
            "/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.viewpager.widget.ViewPager/android.widget.RelativeLayout/android.view.View";

    // playlist test
    private String accId_btnAddToPlaylist = "Add To Playlist";
    private String id_playlistNameInput = "org.schabi.newpipe:id/playlist_name";
    private String xpath_btnCreate = "//*[@text=\"CREATE\"]";
    private String accId_btnNavigateUp = "Navigate up";
    private String accId_btnPlaylistsTab = "Bookmarked Playlists";
    private String xpath_btnDelete = "//*[@text=\"DELETE\"]";
    private String noPlaylistsMessage = "Nothing here but crickets";

    // background playback test
    private String accId_btnBackgroundPlayback = "Audio";
    private String id_btnPlayPause = "org.schabi.newpipe:id/notificationPlayPause";

    // search test
    private String accId_btnSearch = "Search";
    private String id_etSearch = "org.schabi.newpipe:id/toolbar_search_edit_text";
    private String searchTerm = "idubbbz";
    private String accId_btnMoreOptions = "More options";
    private String xpath_btnChannels = "//*[@text=\"Channels\"]";
    private String expectedSearchResult = "iDubbbzTV";

    // about test
    private String accId_btnDrawer = "Open Drawer";
    private String xpath_btnAbout = "//*[@text=\"About\"]";
    private String accId_licensesTab = "Licenses";
    private String id_btnReadLicense = "org.schabi.newpipe:id/app_read_license";
    private String id_dialogTitle = "org.schabi.newpipe:id/alertTitle";
    private String expectedDialogTitle = "GNU General Public License, Version 3.0";
    private String id_btnDialogOk = "android:id/button2";

    // download test
    private String xpath_video = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout" +
            "/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout" +
            "/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout" +
            "/androidx.viewpager.widget.ViewPager/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView" +
            "/android.widget.RelativeLayout[1]"; // change [1] to any number greater than 0 to change the clicked video
    private String accId_btnDownload = "Download stream file";
    private String id_btnPermissionAllow = "com.android.packageinstaller:id/permission_allow_button";
    private String id_etFileName = "org.schabi.newpipe:id/file_name";
    private String id_btnOk = "org.schabi.newpipe:id/okay";
    private String xpath_downloadFinished = "//*[@text=\"Download finished\"]";

    //subject to change, based on device!
    private String videoDownloadLocation = "/storage/emulated/0/Movies/NewPipe/";

    @Test
    public void downloadTest() {
        // click on a video, click the download button and allow storage permission
        findElementWhenVisible(By.xpath(xpath_video)).click();
        findElementWhenVisible(MobileBy.AccessibilityId(accId_btnDownload)).click();
        findElementWhenVisible(By.id(id_btnPermissionAllow)).click();

        // input a random filename to avoid clashing with existing videos
        MobileElement fileNameInput = findElementWhenVisible(By.id(id_etFileName));
        fileNameInput.clear();
        String randomFileName = RandomStringUtils.randomAlphanumeric(10);
        fileNameInput.sendKeys(randomFileName);

        // click the OK button to begin downloading
        findElementWhenVisible(By.id(id_btnOk)).click();

        // open the notifications and wait until the file downloads (could take a while based on size of video)
        driver.openNotifications();
        waitUntilElementVisible(By.xpath(xpath_downloadFinished), 240);

        // when download is finished, check to see if downloaded file exists in storage
        videoDownloadLocation += (randomFileName + ".mp4");
        byte[] downloadedVideo = driver.pullFile(videoDownloadLocation);
        Assert.assertNotNull(downloadedVideo);
    }

    @AfterTest
    public void deleteVideoFromLocalStorage(){
        // based on this great tutorial: https://appiumpro.com/editions/3
        // note: appium server must have --relaxed-security enabled, otherwise adb commands wont work
        List<String> removeDownloadedVideoArgs = Arrays.asList(videoDownloadLocation);
        Map<String, Object> removeVideoCmd = ImmutableMap.of("command", "rm", "args", removeDownloadedVideoArgs);
        driver.executeScript("mobile: shell", removeVideoCmd);
    }

    @Test
    public void addToPlaylistAndDeleteTest() {
        // click the first video
        findElementWhenVisible(By.xpath(firstVideoXpath)).click();
        // click the add to playlist button
        findElementWhenVisible(MobileBy.AccessibilityId(accId_btnAddToPlaylist)).click();

        // enter random playlist name
        MobileElement playlistNameInput = findElementWhenVisible(By.id(id_playlistNameInput));
        playlistNameInput.clear();
        String randomPlaylistName = RandomStringUtils.randomAlphanumeric(10);
        playlistNameInput.sendKeys(randomPlaylistName);

        // click the button to create playlist
        findElementWhenVisible(By.xpath(xpath_btnCreate)).click();
        // navigate back
        findElementWhenVisible(MobileBy.AccessibilityId(accId_btnNavigateUp)).click();
        // click the playlist tab
        findElementWhenVisible(MobileBy.AccessibilityId(accId_btnPlaylistsTab)).click();

        // find the created playlist and assert if it exists
        MobileElement playlist = findElementWhenVisible(By.xpath("//*[@text=\"" + randomPlaylistName + "\"]"));
        Assert.assertEquals(playlist.getText(), randomPlaylistName);

        // long press the created playlist
        AndroidTouchAction touch = new AndroidTouchAction (driver);
        touch.longPress(LongPressOptions.longPressOptions()
                .withElement (ElementOption.element (playlist)))
                .perform ();
        // when the delete dialog appears, press the delete button
        findElementWhenVisible(By.xpath(xpath_btnDelete)).click();

        // wait for dialog to disappear and assert that no playlists are in list
        String screenMessage = findElementWhenVisible(By.xpath("//*[@text=\"" + noPlaylistsMessage + "\"]")).getText();
        Assert.assertEquals(screenMessage, noPlaylistsMessage);
    }

    @Test
    public void backgroundPlaybackTest(){
        // click the first video
        findElementWhenVisible(By.xpath(firstVideoXpath)).click();
        // click the background playback button
        findElementWhenVisible(MobileBy.AccessibilityId(accId_btnBackgroundPlayback)).click();
        // open the notification shade
        driver.openNotifications();
        // assert that the play/pause button isn't null (if it is, the background playback failed)
        Assert.assertNotNull(findElementWhenVisible(By.id(id_btnPlayPause), 30));
    }

    @Test
    public void searchTest(){
        // click the search icon
        findElementWhenVisible(MobileBy.AccessibilityId(accId_btnSearch)).click();

        // clear the search edit text, send the search term and press enter
        MobileElement search = findElementWhenVisible(By.id(id_etSearch));
        search.clear();
        search.sendKeys(searchTerm);
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));

        // expand the options menu and select the Channels option
        findElementWhenVisible(MobileBy.AccessibilityId(accId_btnMoreOptions)).click();
        findElementWhenVisible(By.xpath(xpath_btnChannels)).click();

        // assert that the search results match the expected results
        String searchResult = findElementWhenVisible(By.xpath("//*[@text=\"" + expectedSearchResult + "\"]")).getText();
        Assert.assertEquals(searchResult, expectedSearchResult);
    }

    @Test
    public void readLicenseTest(){
        // open the navigation drawer
        findElementWhenVisible(MobileBy.AccessibilityId(accId_btnDrawer)).click();
        // click the about button
        findElementWhenVisible(MobileBy.xpath(xpath_btnAbout)).click();
        // click the licenses tab
        findElementWhenVisible(MobileBy.AccessibilityId(accId_licensesTab)).click();
        // click the read license button
        findElementWhenVisible(By.id(id_btnReadLicense)).click();

        // verify that the license dialog is opened
        String licenseTitle = findElementWhenVisible(By.id(id_dialogTitle)).getText();
        Assert.assertEquals(licenseTitle, expectedDialogTitle);

        // click the ok button and praise gnu
        findElementWhenVisible(By.id(id_btnDialogOk)).click();
    }
}
