package base;

import common.Constants;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;

public abstract class BaseTest {

    protected AndroidDriver driver;

    @BeforeClass()
    public void setUp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("app", Constants.APK_LOCATION);
        capabilities.setCapability("VERSION", Constants.ANDROID_VERSION);
        capabilities.setCapability("deviceName", Constants.DEVICE_NAME);
        capabilities.setCapability("platformName","Android");
        driver = new AndroidDriver(new URL(Constants.REMOTE_ADDRESS), capabilities);
    }

    @AfterMethod
    public void goToAppHomeScreen() {
        // closes notification shade
        driver.pressKey(new KeyEvent(AndroidKey.BACK));
        driver.launchApp();
    }

    protected void waitUntilElementVisible(By by, long timeoutInSeconds){
        new WebDriverWait(driver, timeoutInSeconds).until(
                ExpectedConditions.visibilityOfElementLocated(by)
        );
    }

    protected MobileElement findElement(By by) {
        return (MobileElement) driver.findElement(by);
    }

    /**
     *  Waits timeoutInSeconds amount of time before trying to find element and returning it.
     * @return MobileElement when found
     */
    protected MobileElement findElementWhenVisible(By by, long timeoutInSeconds){
        waitUntilElementVisible(by, timeoutInSeconds);
        return findElement(by);
    }

    /**
     * Waits 15 seconds for element to become visible and returns it.
     * @return MobileElement when found
     */
    protected MobileElement findElementWhenVisible(By by){
        return findElementWhenVisible(by, 15);
    }
}
